export interface IClientModel {
  // unique model identifier
  // must be only undefined, uuid4 or integer
  id?: string;
}

export interface IPassword extends IClientModel {
  value: string;
  expired: Date;
};

export interface ISession extends IClientModel {
  key?: string;
}

export interface IGroup extends IClientModel {
  name: string;
  key: string;
}

export interface IAccount extends IClientModel {
  email?: string;
  name?: string;
  active?: boolean;
  lastLogin?: string;
  session?: ISession;
  passwords?: IPassword[];
}

export interface IEntry extends IClientModel {
  link: string;
  groupKey: string;
  username?: string;
  description?: string;
  password?: string;
  email?: string;
  firstName?: string;
  lastName?: string;

  viewState?: Array<string>;
}

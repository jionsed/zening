import * as redux from 'redux';

export interface ColumnsVisibilityState {
    username?: boolean;
    password?: boolean;
    description?: boolean;
}

export interface IStorageState {
  groupId?: string;
  visibilityState?: ColumnsVisibilityState;
};

export interface IStorageAction extends redux.Action, IStorageState {};

export const StorageAction = {
  GROUP_CHANGED: 'group-changed',
  VISIBLE_COLUMNS_CHANGED: 'columns-visibility-changed',

  creator: {
    changeGroup: (groupId: string): IStorageAction => { return {type: StorageAction.GROUP_CHANGED, groupId}; },
    changeVisibilityState: (visibilityState: ColumnsVisibilityState): IStorageAction => {
      return {type: StorageAction.VISIBLE_COLUMNS_CHANGED, visibilityState};
    }
  }
};

function defaultState(): IStorageState {
  return {
    groupId: null,
    visibilityState: {
      username: true,
      password: false,
      description: true
    }
  };
}

export function StorageReducer(state: IStorageState = defaultState(), action: IStorageAction): IStorageState {
  switch (action.type) {
    case StorageAction.GROUP_CHANGED:
      return {groupId: action.groupId || null, visibilityState: state.visibilityState};
    case StorageAction.VISIBLE_COLUMNS_CHANGED:
      return {visibilityState: action.visibilityState, groupId: state.groupId};
  }
  return state;
}

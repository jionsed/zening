import { List } from 'immutable';
import * as redux from 'redux';

import { IEntry } from '../models';

export interface IEntryAction extends redux.Action {
  entry?: IEntry;
  list?: List<IEntry>;
  indexes?: Array<number>;
};

export const EntryActions = {
  RELOAD: 'entries-list-reloaded',
  UPDATE: 'entry-update',
  NEW: 'entry-new',
  DELETE_MANY: 'entry-delete-many',

  creator: {
    newEntry: (entry: IEntry): IEntryAction => { return {type: EntryActions.NEW, entry}; },
    reload: (list: List<IEntry>): IEntryAction => { return {type: EntryActions.RELOAD, list}; },
    update: (entry: IEntry): IEntryAction => { return {type: EntryActions.UPDATE, entry}; },
    deleteMany: (indexes: Array<number>): IEntryAction => { return {type: EntryActions.DELETE_MANY, indexes}; }
  }
};

export function EntriesReducer(state: List<IEntry> = List([]), action: IEntryAction) {
  switch (action.type) {
    case EntryActions.NEW:
      return state.push(action.entry);  
    case EntryActions.RELOAD:
      return action.list;
    case EntryActions.UPDATE:
      const index = state.findIndex(value => action.entry.id === value.id);
      return state.set(index, action.entry);
    case EntryActions.DELETE_MANY:
      const data = state.toArray();
      action.indexes.sort().reverse().forEach((index) => {
        if (index !== null)
          data.splice(index, 1);
      });
      return List(data);
  }
  return state;
}

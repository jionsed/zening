import * as redux from 'redux';
import { IAccount } from '../models';

export interface IAccountAction extends redux.Action {
  account: IAccount;
}

export const AccountActions = {
  LOGIN: 'acc-login',
  REGISTER: 'acc-new',
  LOGOUT: 'acc-logout',

  creator: {
    login: (account: IAccount): IAccountAction => { return { type: AccountActions.LOGIN, account }; },
    register: (account: IAccount): IAccountAction => { return { type: AccountActions.REGISTER, account }; },
    logout: (): IAccountAction => { return { type: AccountActions.LOGOUT, account: {} }; }
  }
};

export function AccountReducer(state: IAccount = { passwords: [] }, action: IAccountAction) {
  switch (action.type) {
    case AccountActions.LOGIN:
      return action.account;
    case AccountActions.REGISTER:
      return action.account;
    case AccountActions.LOGOUT:
      return {};
  }
  return state;
}

import { List } from 'immutable';
import * as redux from 'redux';
import { routerReducer, RouterState } from 'react-router-redux';

import { AccountReducer } from './account';
import { GroupsReducer } from './groups';
import { StorageReducer, IStorageState } from './app-storage';
import { EntriesReducer } from './entries';
import { IGroup, IAccount, IEntry } from '../models';

export interface ApplicationState {
  groups: List<IGroup>;
  account: IAccount;
  routing: RouterState;
  storage: IStorageState;
  entries: List<IEntry>;
};

export { IAccountAction, AccountActions } from './account';
export { IEntryAction, EntryActions } from './entries';
export { IGroupAction, GroupActions } from './groups';
export { IStorageAction, StorageAction } from './app-storage';

export const RootStore = redux.createStore(redux.combineReducers({
  account: AccountReducer,
  groups: GroupsReducer,
  storage: StorageReducer,
  entries: EntriesReducer,
  routing: routerReducer
}));

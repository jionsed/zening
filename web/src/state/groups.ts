import { List } from 'immutable';
import * as redux from 'redux';

import { uuid4 } from '../environment';
import { IGroup } from '../models';

export interface IGroupAction extends redux.Action {
  group?: IGroup;
  key?: string;
  stateList?: Array<IGroup>;
};

export const GroupActions = {
  CREATE: 'group-new',
  RENAME: 'group-rename',
  DELETE: 'group-delete',
  RELOAD: 'groups-reloaded',
  UPDATE: 'group-update',

  creator: {
    create: (group: IGroup): IGroupAction => { return { type: GroupActions.CREATE, group }; },
    delete: (uuid: string): IGroupAction => { return { type: GroupActions.DELETE, key: uuid }; },
    rename: (group: IGroup): IGroupAction => { return { type: GroupActions.RENAME, group }; },
    reload: (stateList: Array<IGroup>): IGroupAction => { return { type: GroupActions.RELOAD, stateList }; },
    update: (group: IGroup): IGroupAction => { return { type: GroupActions.UPDATE, group }; }
  }
};

export function GroupsReducer(state: List<IGroup> = List([]), action: IGroupAction) {
  switch (action.type) {
    case GroupActions.CREATE:
      return state.push(action.group);
    case GroupActions.RENAME:
      return state;
    case GroupActions.DELETE:
      return state.remove(state.findIndex(item => item.key === action.key));
    case GroupActions.RELOAD:
      return List(action.stateList);
    case GroupActions.UPDATE:
      return state.update(
        state.findIndex(item => item.key === action.group.key),
        (item) => { return action.group; }
      );
  }
  return state;
}

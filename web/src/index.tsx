import * as alertify from 'alertify.js';
import * as cookie from 'js-cookie';

import * as React from "react";
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import Api from './api';
import { Sidebar } from './views/components';
import * as pages from './views/pages';
import { CProps, CState } from './environment';
import { RootStore, IAccountAction, AccountActions } from './state';

export class App extends React.Component<CProps, CState> {
  constructor() {
    super();

    alertify.logPosition('top right');
    if (cookie.get('pid') !== undefined)
      Api.Account.GetInfo(cookie.get('pid'), cookie.get('uid'))
        .then((data) => {
          RootStore.dispatch<IAccountAction>(AccountActions.creator.login(data));
        }).catch((message) => { });
  }

  render() {
    return (
      <div id="app-root">
        <Sidebar />
        {this.props.children}
      </div>
    );
  }
}

const history = syncHistoryWithStore(hashHistory, RootStore);

ReactDOM.render(
  <Provider store={RootStore}>
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={pages.IndexPage} />
        <Route path="/storage" component={pages.StoragePage} />
        <Route path="/storage/show/:uuid" component={pages.StoragePage} />
        <Route path="/account" component={pages.AccountPage} />
        <Route path="/join" component={pages.JoinPage} />
        <Route path="/rpc-api-docs" component={pages.RpcApiDocsPage} />
      </Route>
    </Router>
  </Provider>,
  document.getElementsByTagName("div")[0]
);

import * as models from './models';
import { RpcService, RestService, encrypt, forEachProperty } from './environment';

export const JsonRpcApi = {
  Account: {
    Activate(password: string, key: string) {
      return RpcService.send("Account.Activate", { password, key });
    },
    GetInfo(password: string, sessionKey: string) {
      return RpcService.send("Account.GetInfo", { password, sessionKey });
    },
    Login(email: string, password: string) {
      return RpcService.send("Account.Login", { email, password });
    },
    Register(email: string, password: string) {
      return RpcService.send("Account.Create", { email, password });
    },
    Update(params: models.IAccount) {
      return RpcService.send("Account.Update", params);
    }
  },
  Groups: {
    GetAll(getParams?: any) {
      return RpcService.send("Groups.GetAll", undefined, getParams);
    },
    Remove(key: string) {
      return RpcService.send("Groups.Remove", { key });
    },
    New(name: string, key: string) {
      return RpcService.send("Groups.New", { name, key });
    },
    Update(key: string, name: string) {
      return RpcService.send("Groups.Update", { key, name });
    }
  },
  WebProfiles: {
    New(params: models.IEntry) {
      forEachProperty(params, ["password", "email", "username"], encrypt);
      return RpcService.send("WebProfiles.New", params);
    },
    Update(params: models.IEntry) {
      forEachProperty(params, ["password", "email", "username"], encrypt);
      return RpcService.send("WebProfiles.Update", params);
    },
    GetAll(groupKey: string, getProps: any) {
      return RpcService.send("WebProfiles.GetAll", { groupKey }, getProps);
    },
    Delete(groupKey: string, id: string) {
      return RpcService.send("WebProfiles.Delete", { id, groupKey });
    },
    Count(groupKey: string) { return RpcService.send("WebProfiles.Count", { groupKey }); }
  }
};

export default {
  Account: {
    Activate(password: string, key: string) {
      return RestService.send("GET", "/account/activate", null, { password, key });
    },
    GetInfo(password: string, sessionKey: string) {
      return RestService.send("GET", "/account/get_info", null, { password, sessionKey });
    },
    Login(email: string, password: string) {
      return RestService.send("POST", "/account/login", { email, password });
    },
    Register(email: string, password: string) {
      return RestService.send("POST", "/account", { email, password });
    },
    Update(params: models.IAccount) {
      return RestService.send("POST", "/account/update", params);
    }
  },
  // ### TODO
  Groups: {
    GetAll(getParams?: any) {
      return RestService.send("GET", "/groups", null, getParams);
    },
    Remove(key: string) {
      return RestService.send("DELETE", "/groups/" + key);
    },
    New(name: string, key: string) {
      return RestService.send("POST", "/groups", { name, key });
    },
    Update(key: string, name: string) {
      return RestService.send("POST", "/groups/" + key, { name });
    }
  },
  WebProfiles: {
    New(params: models.IEntry) {
      forEachProperty(params, ["password", "email", "username"], encrypt);
      return RestService.send("POST", "/groups/" + params.groupKey + "/bookmarks", params);
    },
    Update(params: models.IEntry) {
      forEachProperty(params, ["password", "email", "username"], encrypt);
      return RestService.send("POST", "/groups/" + params.groupKey + "/bookmarks/" + params.id, params);
    },
    GetAll(groupKey: string, getProps: any) {
      return RestService.send("GET", "/groups/" + groupKey + "/bookmarks", null, getProps);
    },
    Delete(groupKey: string, id: string) {
      return RestService.send("DELETE", "/groups/" + groupKey + "/bookmarks/" + id);
    },
    Count(groupKey: string) { return RestService.send("GET", "/groups/" + groupKey + "/bookmarks/count"); }
  }
}; 

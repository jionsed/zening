import * as alertify from 'alertify.js';
import * as crypto from 'crypto-js';
import * as $ from 'jquery';
import * as cookie from 'js-cookie';
import { DispatchProp } from "react-redux";

export interface CProps extends DispatchProp<any> { };

export interface CState { };

export enum InputState {
  Valid,
  Info,
  Warning,
  Error
};

export function decrypt(line: string): string {
  return crypto.AES.decrypt(line, cookie.get("unsafePid")).toString(crypto.enc.Utf8);
}

export function encrypt(line: string): string {
  return crypto.AES.encrypt(line, cookie.get("unsafePid")).toString();
}

export function forEachProperty<T>(element: T, props: Array<string>, cb: Function): T {
  for (let i = 0; i < props.length; i++)
    if (element[props[i]] !== undefined)
      element[props[i]] = cb(element[props[i]]);
  return element;
}

export function uuid4(): string {
  var random: number = 0;
  const chars = '0123456789ABCDEF'.split('');
  const id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.split('');

  id[0] = chars[(random = Math.random() * 0x100000000) & 0xf];
  for (let i = 1; i < 8; i++)
    id[i] = chars[(random >>>= 4) & 0xf];

  id[9] = chars[(random = Math.random() * 0x100000000) & 0xf];
  for (let i = 10; i < 18; i++)
    id[i] = chars[(random >>>= 4) & 0xf];

  id[19] = chars[(random = Math.random() * 0x100000000) & 0x3 | 0x8];
  for (let i = 20; i < 28; i++)
    id[i] = chars[(random >>>= 4) & 0xf];

  id[28] = chars[(random = Math.random() * 0x100000000) & 0xf];
  for (let i = 29; i < 36; i++)
    id[i] = chars[(random >>>= 4) & 0xf];

  return id.join('');
}

export function alertOnError(message) {
  alertify.log(message);
}

export class AsyncChain<T> {

  onChainEnd: () => void;
  onEachPromiseSuccess: (result: any, chainItem: T) => void;
  onEachPromiseError: (error: any, chainItem: T) => void;

  private collection: Array<T>;
  private index: number;

  constructor(collection?: Array<T>) {
    this.reset(collection);
  }

  reset(collection: Array<T>) {
    this.collection = collection;
    this.index = 0;
  }

  map(onEachItem: (val: T) => Promise<any>) {
    if (this.collection === undefined)
      throw { message: "Collection to chain is empty" };

    if (this.index >= this.collection.length) {
      if (this.onChainEnd !== undefined)
        this.onChainEnd();
      return;
    }

    const item = this.collection[this.index];
    this.index += 1;
    onEachItem(item)
      .then((result) => {
        if (this.onEachPromiseSuccess !== undefined)
          return this.onEachPromiseSuccess(result, item);
        else
          return result;
      })
      .catch((error) => {
        if (this.onEachPromiseError !== undefined)
          return this.onEachPromiseError(error, item);
        else
          return error;
      })
      .then(() => {
        this.map(onEachItem);
      });
  }

}

export class RpcService {
  public static point: string = "/api/rpc?";

  static send(method: string, postForm?: any, getArgs: any = {}): Promise<any> {
    let key: string = null;

    const call: any = {
      "json-rpc": "2.0",
      id: uuid4(),
      data: {
        method: method
      }
    };
    getArgs.auth = cookie.get("uid");
    if (postForm !== undefined)
      call.data.params = postForm;

    return new Promise<any>(function (resolve, reject) {
      $.ajax(RpcService.point + $.param(getArgs), {
        contentType: "application/json",
        data: JSON.stringify(call),
        dataType: "json",
        method: "POST",
        timeout: 2500
      })
        .done((data) => {
          let response = data;

          if (data.id === undefined)
            response = JSON.parse(data);

          if (response.id === null)
            return reject("Request form invalid, try again later");

          if (response.id !== call.id)
            return reject("Invalid call id");

          if (response.error && response.error.message)
            return reject(response.error.message);

          if (response.data !== undefined)
            return resolve(response.data);

          resolve({});
        })
        .fail((jqXHR, textStatus, message) => {
          reject(message);
        });
    });
  }
}

export class RestService {

  static point = "/api/rest";

  static send(httpMethod: string, path: string, postForm?: any, getArgs: any = {}) {
    getArgs.auth = cookie.get("uid");

    return new Promise<any>(function (resolve, reject) {
      RestService[httpMethod]([RestService.point, path, '?', $.param(getArgs)].join(''), getArgs, postForm)
        .done((data) => {
          let response = data;
          resolve(response);
        })
        .fail((jqXHR, textStatus, message) => {
          reject(message);
        });
    });
  }

  static GET(path: string) {
    return $.ajax(path, {
      method: "GET",
      dataType: "json",
      contentType: "application/json",
    });
  }

  static POST(path: string, args: any, form: any) {
    return $.ajax(path, {
      contentType: "application/json",
      data: JSON.stringify(form),
      dataType: "json",
      method: "POST",
      timeout: 2500
    })
  }

}

export function highlightInput(state: InputState, ...inputs: HTMLInputElement[]) {
  let className: string = "warning-highlight";

  if (state === InputState.Error)
    className = "error-highlight";
  else if (state === InputState.Valid)
    className = "valid-highlight";

  inputs.forEach((el) => {
    el.classList.add(className)
  });
}

export function hasMinLenght(minLen: number, ...inputs: HTMLInputElement[]): boolean {
  for (let i = 0; i < inputs.length; i++)
    if (inputs[i].value.length < minLen)
      return false;
  return true;
}

export interface BookmarkItem {
  link: string;
  description: string;
  fullDescription?: string;
};

export interface BookmarkGroup {
  name?: string;
  items: Array<BookmarkItem>;
  parent: BookmarkGroup;
};

export class BookmarkParser {
  /**
   * file content compatible with Netscape Bookmark File Format
   */
  reader: LineReader;
  entriesCount: number;
  onCompleted: (result: Array<BookmarkGroup>) => void;

  constructor(file: File) {
    this.reader = new LineReader(file);
    this.entriesCount = 0;
  }

  parse(skipEmpty: boolean) {
    let groupName = "Imported bookmarks root";
    let current: BookmarkGroup = {
      items: [],
      parent: null
    };
    const result: Array<BookmarkGroup> = [];

    this.reader.onEachLine = (line) => {
      if (current === null) {
        return;
      }

      if (BookmarkParser.isEntry(line)) {
        this.entriesCount += 1;
        return current.items.push({
          link: BookmarkParser.attribute(line, "HREF"),
          description: BookmarkParser.innerText(line)
        });
      }

      if (BookmarkParser.isGroupName(line)) {
        groupName = BookmarkParser.innerText(line);
        return;
      }

      if (BookmarkParser.isGroup(line)) {
        current = {
          items: [],
          parent: current,
          name: groupName
        };
        return result.push(current);
      }

      if (BookmarkParser.isGroupEnd(line)) {
        current = current.parent;
        return;
      }

      if (BookmarkParser.isDescription(line)) {
        current.items[current.items.length - 1].fullDescription = BookmarkParser.description(line);
        return;
      }

      if (line.length > 0 && current.items.length > 0)
        current.items[current.items.length - 1].fullDescription += line;
    };

    this.reader.onReadingEnd = () => {
      /**
       * remove empty elements from parsed array
       */
      if (skipEmpty) {
        const emptyGroups = [];
        // search for empty items
        for (let i = result.length - 1; i >= 0; i--)
          if (result[i].items.length === 0)
            emptyGroups.push(i);

        // delete elements by index
        emptyGroups.forEach(index => result.splice(index, 1));
      }

      if (this.onCompleted !== undefined)
        this.onCompleted(result);
    };
    this.reader.process();
  }

  static attribute(line: string, attrName: string): string {
    let i = line.indexOf(attrName);
    return line.substring(
      i + attrName.length + 2,
      line.indexOf('"', i + attrName.length + 3)
    );
  }

  static description(line: string): string {
    return line.substring(line.indexOf('DD') + 3);
  }

  static isEntry(line: string): boolean {
    return line.indexOf("<DT><A") > -1;
  }

  static isGroup(line: string): boolean {
    return line.indexOf("<DL") > -1;
  }

  static isGroupName(line: string): boolean {
    return line.indexOf("<H3") > -1;
  }

  static isGroupEnd(line: string): boolean {
    return line.indexOf("</DL") > -1;
  }

  static isDescription(line: string): boolean {
    return line.indexOf("<DD") > -1;
  }

  static innerText(line: string): string {
    return line.substring(line.lastIndexOf('">') + 2, line.lastIndexOf('</'));
  }
}

export class LineReader {
  buffer: Array<string>;
  file: File;
  onEachLine: (line: string) => void;
  onReadingEnd: () => void;;

  /**
   * current position in file
   */
  position: number;
  readonly chunkLen: number;

  private reader: FileReader;
  private workPool: Array<Function>;
  private maxLines: number;

  constructor(file: File) {
    this.position = 0;
    this.chunkLen = 128;
    this.file = file;
    this.buffer = [];
    this.workPool = [];

    this.reader = new FileReader();
    this.reader.onload = (ev) => {
      this.parseChunk((ev.target as FileReader).result as string);

      if (!this.hasNext()) {
        this.onEachLine(this.buffer.join('').trim());
        this.onReadingEnd();
      }

      if (this.maxLines !== undefined)
        this.maxLines -= 1;

      if (this.workPool.length > 0)
        this.workPool.pop()();
      else if (this.hasNext())
        this.process(this.maxLines);
    };
  }

  hasNext(): boolean {
    return this.position <= this.file.size;
  }

  parseChunk(chunk: string) {
    const index = chunk.indexOf('\n');

    if (index === -1)
      return this.buffer.push(chunk);

    this.buffer.push(chunk.substring(0, index + 1));
    this.onEachLine(this.buffer.join('').trim());
    this.buffer.length = 0;

    this.parseChunk(chunk.substring(index + 1, chunk.length));
  }

  process(maxLines?: number): void {
    this.maxLines = maxLines;
    if (maxLines === 0) {
      this.maxLines = undefined;
      return;
    }

    this.workPool.push(() => {
      const blob = this.file.slice(this.position, this.position + this.chunkLen);
      this.reader.readAsText(blob, 'utf-8');
      this.position += this.chunkLen;
    });

    if (this.reader.readyState === 2 || this.reader.readyState === 0)
      this.workPool.pop()();
  }
}

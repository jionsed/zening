import * as alertify from 'alertify.js';
import * as cookie from 'js-cookie';
import * as React from "react";
import * as ReactDOM from "react-dom";
import { hashHistory } from 'react-router';

import Api from '../../api';
import * as Core from '../../environment';
import { IAccount } from '../../models';
import { RootStore, AccountActions, IAccountAction } from '../../state';

export interface LoginFormState extends Core.CState {
  keepUser: boolean;
}

export class LoginForm extends React.Component<any, LoginFormState> {
  email: HTMLInputElement;
  password: HTMLInputElement;

  constructor() {
    super();

    this.state = {
      keepUser: false
    };
  }

  render() {
    return (
      <form id="login-form" className="pure-form pure-form-stacked">
        <input name="email" type="email" onKeyUp={this.formKeyUp.bind(this)} ref={(input) => this.email = input} placeholder="Account email" required />
        <input name="password" type="password" onKeyUp={this.formKeyUp.bind(this)} ref={(input) => this.password = input} placeholder="Password" required />
        <label id="keep-user-label" htmlFor="keep-user">
          <input id="keep-user" checked={this.state.keepUser} onChange={this.handleChange.bind(this)} type="checkbox" />
          <span>Remember me</span><br />
          <span>*Unsafe action</span>
        </label>
        <input type="button" value="Log in" onClick={this.login.bind(this)} className="pure-button pure-button-primary" />
      </form>
    );
  }

  formKeyUp(event) {
    if (event.keyCode === 13)
      this.login();
  }

  login() {
    if (!Core.hasMinLenght(5, this.email))
      return Core.highlightInput(Core.InputState.Error, this.email);
    if (!Core.hasMinLenght(6, this.password))
      return Core.highlightInput(Core.InputState.Error, this.password);

    const props = { expires: 1, path: '/' };
    if (this.state.keepUser) {
      props.expires = 360;
      cookie.set("unsafePid", this.password.value, props);
    }

    Api.Account.Login(this.email.value, this.password.value)
      .then((data: IAccount) => {

        cookie.set("uid", data.session.key, props);
        cookie.set("pid", data.passwords[0].value, props);

        RootStore.dispatch<IAccountAction>(AccountActions.creator.login(data));
        hashHistory.push('/storage');
      }).catch((message: string) => {
        alertify.log(message);
      });
  }

  handleChange(event) {
    this.setState({ keepUser: event.target.checked });
  }
}

import * as alertify from 'alertify.js';
import * as React from "react";
import * as ReactDOM from "react-dom";

import Api from '../../api';
import * as Core from '../../environment';
import { RootStore, AccountActions, IAccountAction } from '../../state';

export class NewUserForm extends React.Component<any, Core.CState> {
  email: HTMLInputElement;
  password: HTMLInputElement;
  password2: HTMLInputElement;

  constructor() {
    super();
  }

  render() {
    return (
      <form id="new-user-form" className="pure-form pure-form-stacked">
        <input name="email" type="email" onKeyUp={this.formKeyUp.bind(this)}
          ref={(input) => this.email = input} placeholder="Account email" required />
        <input name="password" type="password" onKeyUp={this.formKeyUp.bind(this)}
          ref={(input) => this.password = input} placeholder="Password" required />
        <input name="password2" type="password" onKeyUp={this.formKeyUp.bind(this)}
          ref={(input) => this.password2 = input} placeholder="Repeat password" required />
        <input type="button" value="Create" onClick={this.register.bind(this)}
          className="pure-button pure-button-primary" />
      </form>
    );
  }

  formKeyUp(event) {
    if (event.keyCode === 13)
      this.register();
  }

  register() {
    if (!Core.hasMinLenght(5, this.email))
      return Core.highlightInput(Core.InputState.Error, this.email);
    if (!Core.hasMinLenght(6, this.password, this.password2))
      return Core.highlightInput(Core.InputState.Error, this.password, this.password2);

    if (this.password.value === this.email.value)
      return Core.highlightInput(Core.InputState.Error, this.password, this.email);

    if (this.password.value !== this.password2.value)
      return Core.highlightInput(Core.InputState.Error, this.password, this.password2);

    Api.Account.Register(this.email.value, this.password.value)
      .then((data: any) => {
        RootStore.dispatch<IAccountAction>(AccountActions.creator.register({}));
        alertify.alert("Great! Check you email for activation code, login and enter him on account page");
      }).catch(Core.alertOnError);
  }
}

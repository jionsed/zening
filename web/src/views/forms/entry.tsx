import * as React from 'react';

import { CProps, CState } from '../../environment';
import { IEntry } from '../../models';

export interface EntryFormProps {
  entry?: IEntry;
  onSave(form): void;
};

export class EntryForm extends React.Component<EntryFormProps, CState> {
  link: HTMLInputElement;
  username: HTMLInputElement;
  email: HTMLInputElement;
  description: HTMLTextAreaElement;
  firstName: HTMLInputElement;
  lastName: HTMLInputElement;
  password: HTMLInputElement;

  render() {
    let defaultEntry = this.props.entry;
    if (this.props.entry === undefined || this.props.entry === null)
      defaultEntry = {
        email: '',
        description: '',
        link: '',
        groupKey: '',
        username: '',
        firstName: '',
        lastName: ''
      };

    return (
      <div className="pure-form pure-form-aligned">
        <div className="pure-control-group">
          <input required type="text" ref={input => this.link = input} defaultValue={defaultEntry.link}
            onKeyUp={this.handleEnter.bind(this)} className="entry-url"
            name="link" placeholder="Page url" />
          <span>*required field</span>
        </div>
        <div className="pure-control-group">
          <input type="email" ref={input => this.email = input} defaultValue={defaultEntry.email}
            onKeyUp={this.handleEnter.bind(this)} name="email"
            className="entry-email" placeholder="Account email" />
          <input type="text" ref={input => this.username = input} defaultValue={defaultEntry.username}
            onKeyUp={this.handleEnter.bind(this)} name="username" placeholder="Account name" />
        </div>
        <div className="pure-control-group">
          <input type="password" ref={input => this.password = input} defaultValue={defaultEntry.password}
            onKeyUp={this.handleEnter.bind(this)} name="password" placeholder="Account password" />
        </div>
        <div className="pure-control-group">
          <textarea ref={input => this.description = input} rows={4}
            defaultValue={defaultEntry.description} onKeyUp={this.handleEnter.bind(this)}
            cols={54} placeholder="Description, useful info, short note, etc"></textarea>
        </div>
        <div className="pure-control-group">
          <input type="text" ref={input => this.firstName = input} defaultValue={defaultEntry.firstName}
            onKeyUp={this.handleEnter.bind(this)} name="email"
            className="entry-firstname" placeholder="Firstname" />
          <input type="text" ref={input => this.lastName = input} defaultValue={defaultEntry.lastName}
            onKeyUp={this.handleEnter.bind(this)} name="lastName"
            className="entry-lastname" placeholder="Lastname" />
        </div>
        <div className="pure-control-group">
          <input type="button" value="Save" className="pure-button pure-button-primary"
            onClick={() => { this.props.onSave(this); }} />
        </div>
      </div>
    );
  }

  handleEnter(event) {
    if (event.keyCode === 13)
      this.props.onSave(this);
  }
}

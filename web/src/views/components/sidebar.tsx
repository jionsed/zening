import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import { AccountMenu } from './account-menu';
import { GroupsList } from './groups-list';
import { MainMenu } from './main-menu';
import { ApplicationState } from '../../state';
import { CState, CProps } from '../../environment';
import { IAccount } from '../../models';

interface SidebarProps extends CProps {
  account: IAccount;
  location: string;
};

const mainMenuLocations = ['/', '/rpc-api-docs'];

class SidebarBase extends React.Component<SidebarProps, CState> {
  render() {
    const isLoggedIn = this.props.account.email !== null && this.props.account.email !== undefined;
    const menuComponent = (mainMenuLocations.indexOf(this.props.location) > -1 || !isLoggedIn) ? <MainMenu /> : <GroupsList />;

    return (
      <div id="menu">
        <div className="pure-menu">
          <AccountMenu />
          {menuComponent}
        </div>
      </div>
    );
  }
}

function propsStateMapper(state: ApplicationState) {
  return {
    account: state.account,
    location: state.routing.locationBeforeTransitions.pathname
  };
}

export const Sidebar = connect(propsStateMapper)(SidebarBase);

import * as alertify from 'alertify.js';
import { List } from 'immutable';

import * as React from 'react';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router';
import { debounce } from 'underscore';

import Api from '../../api';
import { CProps, CState, uuid4, alertOnError } from '../../environment';
import { IGroup } from '../../models';
import { IGroupAction, GroupActions } from '../../state/groups';
import { IStorageAction, StorageAction } from '../../state/app-storage';
import { RootStore, ApplicationState } from '../../state';

/**
 * GroupElement component
 */

enum GroupElementMode {
  Edit,
  View
};

export interface GroupElementState extends CState {
  active: boolean;
  mode: GroupElementMode;
}

export interface GroupElementProps {
  name: string;
  uuid: string;
  onClick?(GroupElement);
  isActive?: boolean;
}

class GroupElement extends React.Component<GroupElementProps, GroupElementState> {
  newName: HTMLInputElement;

  constructor() {
    super();

    this.state = {
      active: false,
      mode: GroupElementMode.View
    };
  }

  render() {
    const isSelected = (this.state.active === true || this.props.isActive);

    // TODO <i onClick={this.deleteGroup.bind(this)} className="fa fa-close icon-button"></i>
    return (
      <div onClick={this.filterClickTarget.bind(this)}
        className={isSelected ? "group-element group-element-active" : "group-element"}>
        {(this.state.mode === GroupElementMode.View) ? (
          <span onKeyUp={this.onEnterPress.bind(this)}>{this.props.name}</span>
        ) : (
            <input ref={input => this.newName = input} type="text"
              onKeyUp={this.applyName.bind(this)} defaultValue={this.props.name} />
          )}
        <i className="fa group-entry-delete icon-button sidebar-icon"></i>
        <i className="fa group-entry-rename icon-button sidebar-icon"></i>
      </div>
    );
  }

  applyName(ev) {
    if (ev.keyCode === 13)
      this.renameGroup();
  }

  filterClickTarget(ev) {
    if (!(ev.target.tagName as string).startsWith("I"))
      return this.props.onClick(this);

    if (ev.target.classList.contains("group-entry-rename"))
      this.renameGroup();
    else if (ev.target.classList.contains("group-entry-delete"))
      this.deleteGroup();
  }

  onEnterPress(event) {
    if (event.keyCode === 13)
      this.props.onClick(this);
  }

  deleteGroup() {
    Api.Groups.Remove(this.props.uuid)
      .then(() => {
        RootStore.dispatch<IGroupAction>(GroupActions.creator.delete(this.props.uuid));
        if (this.state.active === true || this.props.isActive)
          hashHistory.push('/storage');
      }).catch(alertOnError);
  }

  renameGroup() {
    if (this.state.mode === GroupElementMode.View)
      return this.setState({ mode: GroupElementMode.Edit });

    if (this.newName.value === this.props.name)
      return this.setState({ mode: GroupElementMode.View });

    Api.Groups.Update(this.props.uuid, this.newName.value)
      .then((group) => {
        RootStore.dispatch<IGroupAction>(
          GroupActions.creator.update(group)
        );
        this.setState({ mode: GroupElementMode.View });
      }).catch(alertOnError);
  }
}

/**
 * GroupsList component
 */

interface GroupsListProps extends CProps {
  selectedId: string;
  groups: List<IGroup>;
}

interface GroupsListState extends CState {
  searchEnabled: boolean;
}

class GroupsListBase extends React.Component<GroupsListProps, GroupsListState> {
  groupName: HTMLInputElement;
  searchBtn: HTMLElement;

  constructor() {
    super();

    this.state = {
      searchEnabled: false
    };
    this.formKeyUp = debounce(this.formKeyUp, 250);
  }

  componentWillReceiveProps(nextProps: GroupsListProps) {
    if (this.props.selectedId !== nextProps.selectedId)
      this.forceUpdate();
  }

  render() {
    const items = this.props.groups.map((group: IGroup) => {
      const isActive = group.key === this.props.selectedId;
      if (this.state.searchEnabled && group.name.indexOf(this.groupName.value) === -1)
        return;

      return (<GroupElement isActive={isActive} uuid={group.key} key={group.key}
        onClick={this.groupChanged.bind(this)} name={group.name} />);
    });

    return (
      <div id="groups-storage">
        <div id="groups-header" className="pure-form">
          <input className="pure-input-1" type="text" onKeyUp={e => this.formKeyUp(e.keyCode)}
            placeholder="New group name" ref={(input) => this.groupName = input} />
          <i type="text" id="sidebar-search-button" className="fa fa-search icon-button"
            onClick={this.toggleSearch.bind(this)} ref={item => this.searchBtn = item}></i>
          <input type="button" onClick={this.addGroup.bind(this)}
            className="pure-button pure-button-primary" value="Add" />
        </div>
        <div id="groups-list">
          {items}
        </div>
      </div>
    );
  }

  componentDidMount() {
    Api.Groups.GetAll().then((data: Array<IGroup>) => {
      if (data.length === undefined)
        return;

      RootStore.dispatch<IGroupAction>(GroupActions.creator.reload(data));
    }).catch(alertOnError);
  }

  toggleSearch() {
    this.searchBtn.classList.toggle("toggle-btn-active");
    const searchMode = this.searchBtn.classList.contains("toggle-btn-active");
    this.groupName.placeholder = (searchMode) ? "Search enabled" : "New group name";
    this.setState({ searchEnabled: searchMode });
  }

  formKeyUp(keyCode) {
    if (keyCode === 13)
      this.addGroup();
    else if (this.state.searchEnabled)
      this.forceUpdate();
  }

  groupChanged(selectedItem: GroupElement) {
    if (this.props.selectedId === selectedItem.props.uuid)
      return;

    hashHistory.push('/storage/show/' + selectedItem.props.uuid);
    RootStore.dispatch<IStorageAction>(StorageAction.creator.changeGroup(selectedItem.props.uuid));
  }

  addGroup() {
    const name = this.groupName.value;
    if (name.length < 3)
      return;

    const group: IGroup = { name, key: uuid4() };
    RootStore.dispatch<IGroupAction>(GroupActions.creator.create(group));
    Api.Groups.New(name, group.key).catch((message) => {
      alertify.log(message);
      RootStore.dispatch<IGroupAction>(GroupActions.creator.delete(group.key));
    });
    this.groupName.value = "";
  }
}

export const GroupsList = connect((store: ApplicationState) => {
  let id = store.storage.groupId;
  if (store.routing.locationBeforeTransitions.action === "POP"
    && store.routing.locationBeforeTransitions.pathname.indexOf("/show/") === -1)
    id = null;

  return {
    groups: store.groups,
    selectedId: id
  };
})(GroupsListBase);

import * as cookie from 'js-cookie';

import * as React from 'react';
import { connect, DispatchProp } from 'react-redux';
import { Link, hashHistory } from 'react-router';

import { CProps, CState } from '../../environment';
import { IAccount, IGroup } from '../../models';
import { RootStore, AccountActions, IAccountAction } from '../../state';

interface AccountProps extends DispatchProp<any> {
  user: IAccount;
  location: string;
}

class AccountMenuBase extends React.Component<AccountProps, any> {
  render() {
    if (this.props.user.email === undefined)
      return (
        <Link id="userbar-login-menu" className="pure-menu-heading" to="/join">Log in</Link>
      );
    const name = this.props.user.name || this.props.user.email.split('@')[0];

    return (
      <div id="account-menu">
        <Link id="index-menu-item" activeClassName="active-link"
          className="pure-menu-heading" to="/">/</Link>
        <div id="sidebar-userbox-menu">
          <Link id="userbar" activeClassName="active-link"
            className="pure-menu-heading" to="/storage">{name}</Link>
          <Link activeClassName="active-link" className="pure-menu-heading" to="/account">
            <i className="fa fa-gear icon-button sidebar-icon"></i>
          </Link>
          <i id="userbar-logout-button" onClick={this.logout.bind(this)}
            className="fa fa-user-times icon-button sidebar-icon"></i>
        </div>
      </div>
    );
  }

  logout() {
    RootStore.dispatch<IAccountAction>(AccountActions.creator.logout());
    cookie.remove('pid');
    cookie.remove('uid');

    if (this.props.location !== '/join')
      hashHistory.push('/join');
  }
};

export const AccountMenu = connect((store) => {
  return {
    user: store.account,
    location: store.routing.locationBeforeTransitions.pathname
  };
})(AccountMenuBase);

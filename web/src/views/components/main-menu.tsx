import * as React from 'react';
import { Link } from 'react-router';

export function MainMenu() {
  return (
    <ul className="pure-menu-list">
      <li className="pure-menu-item">
        <Link className="pure-menu-link" activeClassName="active-link" to="/rpc-api-docs">RPC api docs</Link>
      </li>
      <li className="pure-menu-item">
        <a className="pure-menu-link" target="_blank"
          href="https://gitlab.com/jionsed/zening">Git repo (new tab)</a>
      </li>
    </ul>
  );
}

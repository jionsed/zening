import * as React from 'react';

import { CState, CProps } from '../../environment';

export interface PaginatorProps {
  itemsCount: number;
  itemsPerPage: number;
  pageChanged?: (index: number) => void;
};

export interface PaginatorState {
  currentPage: number;
};

export class Pagination extends React.Component<PaginatorProps, PaginatorState> {
  pageValue: HTMLInputElement;

  constructor() {
    super();

    this.state = {
      currentPage: 1
    };
  }

  render() {
    const pagesCount = Math.ceil(this.props.itemsCount / this.props.itemsPerPage) || 1;

    return (
      <div className="react-paginator">
        <span onClick={this.prevPage.bind(this)} className={"react-paginator-control " + this.isPrevActive()}>{"<"}</span>
        <input ref={input => this.pageValue = input} defaultValue={this.state.currentPage.toString()}
          type="text" onKeyUp={this.inputValueChanged.bind(this)} className="react-paginator-input" />
        <span>/</span>
        <span>{pagesCount}</span>
        <span onClick={this.nextPage.bind(this)} className={"react-paginator-control " + this.isNextActive()}>{">"}</span>
        <span></span>
      </div>
    );
  }

  inputValueChanged(event) {
    if (event.keyCode !== 13)
      return;

    if (this.pageValue.value === "")
      return this.restoreValue();

    const newVal = parseInt(this.pageValue.value, 10);
    if (isNaN(newVal))
      return this.restoreValue();

    if (this.state.currentPage === newVal)
      return;

    if (newVal > Math.ceil(this.props.itemsCount / this.props.itemsPerPage))
      return this.restoreValue();

    this.setPage(newVal);
  }

  restoreValue() {
    this.pageValue.value = this.state.currentPage.toString();
  }

  setPage(index: number) {
    this.pageValue.value = index.toString();
    if (this.props.pageChanged)
      this.props.pageChanged(index);
    this.setState({ currentPage: index });
  }

  prevPage() {
    if (this.state.currentPage === 1)
      return;
    this.setPage(this.state.currentPage - 1);
  }

  nextPage() {
    if (this.state.currentPage === Math.ceil(this.props.itemsCount / this.props.itemsPerPage))
      return;
    this.setPage(this.state.currentPage + 1);
  }

  isPrevActive() {
    if (this.state.currentPage > 1)
      return "";
    return "react-paginator-control-inactive";
  }

  isNextActive() {
    if (this.state.currentPage !== Math.ceil(this.props.itemsCount / this.props.itemsPerPage))
      return "";
    return "react-paginator-control-inactive";
  }
}

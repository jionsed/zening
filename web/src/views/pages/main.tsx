import * as $ from 'jquery';
import * as React from 'react';
import * as Markdown from 'react-markdown';
import { CProps, CState } from '../../environment';

interface IndexPageState extends CState {
  readme: string;
};

let cachedReadme: string = null;

export class IndexPage extends React.Component<CProps, IndexPageState> {
  constructor() {
    super();

    this.state = {
      readme: ''
    };
  }

  render() {
    return (
      <div className="content" id="index">
        <Markdown source={this.state.readme} />
      </div>
    );
  }

  componentWillMount() {
    if (cachedReadme === null)
      $.get('/root/README.md').then((content) => {
        cachedReadme = content;
        this.setState({ readme: cachedReadme });
      });
    else
      this.setState({ readme: cachedReadme });
  }
}

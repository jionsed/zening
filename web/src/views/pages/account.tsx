import * as alertify from 'alertify.js';
import * as cookie from 'js-cookie';
import * as React from 'react';
import { connect } from 'react-redux';
import * as ProgressBar from 'rc-progress';

import Api from '../../api';
import * as Core from '../../environment';
import { IGroup, IEntry, IAccount } from '../../models';
import { ApplicationState, RootStore } from '../../state';
import { IAccountAction, AccountActions } from '../../state/account';

class ActivationBox extends React.Component<any, Core.CState> {
  keyInput: HTMLInputElement;

  render() {
    return (
      <div id="account-activation-box">
        <input type="text" className="activation-input" onKeyUp={this.onEnterPressed.bind(this)}
          ref={input => this.keyInput = input} placeholder="Key (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx)" />
        <input type="button" className="pure-button pure-button-primary"
          onClick={this.confirmActivation.bind(this)} value="Activate" />
      </div>
    );
  }

  confirmActivation() {
    if (this.keyInput.value.length === 0)
      return;

    Api.Account.Activate(cookie.get('pid'), this.keyInput.value)
      .then((account) => {
        alertify.log('Done! Account activated');
        RootStore.dispatch<IAccountAction>(AccountActions.creator.login(account));
      }).catch(Core.alertOnError);
  }

  onEnterPressed(event) {
    if (event.keyCode == 13)
      this.confirmActivation();
  }
}

export interface ImportBookmarksBoxState extends Core.CState {
  progressBarVisible: boolean;
  entriesToImport: number;
  alreadyImported: number;
}

class ImportBookmarksBox extends React.Component<any, ImportBookmarksBoxState> {
  fileChooser: HTMLInputElement;

  constructor() {
    super();

    this.state = {
      progressBarVisible: false,
      entriesToImport: 1,
      alreadyImported: 1
    };
  }

  render() {
    let progressBarDisplay = "";
    const importProgress = (this.state.alreadyImported / this.state.entriesToImport) * 100;

    if (!this.state.progressBarVisible)
      progressBarDisplay = "display-hidden";

    return (
      <div id="bookmarks-import-box">
        <span>Import links from bookmarks file</span>
        <input type="file" accept=".html" ref={input => this.fileChooser = input} id="bookmarks-import" />
        <div>
          <label>Parse bookmarks on: </label>
          <label>
            <input type="radio" name="parse-on-side" value="c" defaultChecked />
            <span>Client (with progress)</span>
            <div id="import-progress" className={progressBarDisplay}>
              <span>Entries imported: {this.state.alreadyImported}/{this.state.entriesToImport}</span>
              <ProgressBar.Line strokeWidth="4" percent={importProgress} />
            </div>
          </label>
          <label>
            <input type="radio" name="parse-on-side" value="S" /> <span>Server (silent process)</span>
          </label>
        </div>
        <input type="button" onClick={this.importFromBookmarks.bind(this)}
          value="Run" className="pure-button pure-button-primary" />
      </div>
    );
  }

  importFromBookmarks() {
    if (this.fileChooser.files.length === 0)
      return;

    const parser = new Core.BookmarkParser(this.fileChooser.files[0]);
    parser.onCompleted = (result) => {
      this.setState({
        progressBarVisible: true,
        entriesToImport: parser.entriesCount,
        alreadyImported: 0
      });
      // todo save each group with entries
      // todo animate loading process
      const groupsChain = new Core.AsyncChain<Core.BookmarkGroup>(result);
      groupsChain.onEachPromiseSuccess = (group: IGroup, chainItem) => {
        return this.importEntriesGroup(group, chainItem);
      };
      groupsChain.onChainEnd = () => {
        alertify.log('Import completed succefully!');
        this.setState({ progressBarVisible: false });
      };
      groupsChain.map(group => Api.Groups.New(group.name, Core.uuid4()));
    };
    parser.parse(true);
  }

  importEntriesGroup(group: IGroup, chainItem: Core.BookmarkGroup) {
    return new Promise<void>((resolve, reject) => {
      const entriesChain = new Core.AsyncChain<Core.BookmarkItem>(chainItem.items);
      entriesChain.onChainEnd = () => { resolve(); };
      entriesChain.onEachPromiseSuccess = () => {
        this.setState({
          alreadyImported: this.state.alreadyImported + 1
        });
      };
      entriesChain.map((item) => {
        return Api.WebProfiles.New({
          groupKey: group.key,
          link: item.link,
          description: item.description || item.fullDescription
        });
      });
    });
  }

}

export interface AccountPageProps extends Core.CProps {
  account: IAccount;
}

export class AccountPageBase extends React.Component<AccountPageProps, Core.CState> {
  username: HTMLInputElement;

  render() {
    if (this.props.account.email === undefined && this.props.account.active === undefined)
      return (<div className="content" id="account"></div>);
    const username = this.props.account.name || this.props.account.email.split('@')[0];

    return (
      <div className="content" id="account">
        <div className="tab-content pure-form pure-form-stacked">
          <h3>Account settings</h3>
          {!this.props.account.active && (
            <ActivationBox />
          )}
          <div>
            <input type="text" className="pure-input-1" ref={input => this.username = input}
              defaultValue={username} placeholder="Account username"
              onKeyUp={this.onUsernameEntered.bind(this)} />
            <input type="button" value="Apply" onClick={this.applyUsername.bind(this)}
              className="pure-button pure-button-primary" />
          </div>
        </div>
        <div className="tab-content pure-form pure-form-stacked">
          <h3>Data manipulation</h3>
          <ImportBookmarksBox />
        </div>
      </div>
    );
  }

  applyUsername() {
    Api.Account.Update({ name: this.username.value })
      .then((user: IAccount) => {
        RootStore.dispatch<IAccountAction>(AccountActions.creator.login(user));
      }).catch(Core.alertOnError);
  }
  onUsernameEntered(ev) {
    if (ev.keyCode === 13)
      this.applyUsername();
  }
}

function propsStateMapper(state: ApplicationState) {
  return {
    account: state.account
  };
}

export const AccountPage = connect(propsStateMapper)(AccountPageBase);

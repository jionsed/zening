import * as alertify from 'alertify.js';
import { List } from 'immutable';
import * as React from 'react';
import * as Modal from 'react-modal';
import * as Table from 'rc-table';
import { connect } from 'react-redux';
import { debounce } from 'underscore';

import { Pagination } from '../components';
import { EntryForm } from '../forms';
import { CProps, CState, decrypt, forEachProperty } from '../../environment';
import { IGroup, IEntry } from '../../models';

import Api from '../../api';
import { ApplicationState, RootStore } from '../../state';
import { IEntryAction, EntryActions } from '../../state/entries';
import { IGroupAction, GroupActions } from '../../state/groups';
import { IStorageAction, IStorageState, StorageAction, ColumnsVisibilityState } from '../../state/app-storage';

interface StoragePageProps extends CProps {
  params: any;
  entries: List<IEntry>;
  visibleColumns: ColumnsVisibilityState;
};

interface StorePageState extends CState {
  entryDialogVisible: boolean;
  selectedEntry: IEntry;
  deleteDisabled: boolean;
  searchByColumn: string;
  showPasswords: boolean;
  totalEntries: number;
  entriesPerPage: number;
}

export class StoragePageBase extends React.Component<StoragePageProps, StorePageState> {
  checkedRows: Array<string>;
  storeColumns: Array<any>;
  searchInput: HTMLInputElement;

  constructor() {
    super();

    this.state = {
      entryDialogVisible: false,
      selectedEntry: null,
      deleteDisabled: true,
      searchByColumn: 'link',
      showPasswords: false,
      totalEntries: 0,
      entriesPerPage: 20
    };
    this.checkedRows = [];
    this.storeColumns = [{
      title: '',
      key: 'selector',
      width: 32,
      render: (text: string, record: IEntry, index: number) => {
        return <input type="checkbox" />;
      },
      onCellClick: (record: IEntry, event) => {
        if (record.viewState === undefined)
          record.viewState = [];
        if (record.viewState.length > 0)
          record.viewState.length = 0;

        record.viewState.push('interaction');
        if (event.target.tagName !== "INPUT")
          return;

        if (event.target.checked)
          this.checkedRows.push(record.id);
        else
          this.checkedRows.splice(this.checkedRows.indexOf(record.id), 1);

        if (this.checkedRows.length > 0 && this.state.deleteDisabled)
          return this.setState({ deleteDisabled: false });
        if (this.checkedRows.length === 0 && !this.state.deleteDisabled)
          this.setState({ deleteDisabled: true });
      }
    }, {
      title: 'Link', dataIndex: 'link', key: 'link', width: 290,
      render: (val: string, record: IEntry, index: number) => {
        return <a href={val} target="_blank">{val}</a>;
      }
    }, {
      title: 'Email', dataIndex: 'email', key: 'email', width: 170
    }, {
      title: 'Username', dataIndex: 'username', key: 'username', width: 140
    }, {
      title: 'Password', dataIndex: 'password', key: 'password', width: 100
    }, {
      title: 'Description', dataIndex: 'description', key: 'description', width: 400
    }, {
      title: '',
      key: 'hoverIcons',
      width: 48,
      render: (val: string, record: IEntry, index: number) => {
        return <i className="fa icon-entry-edit icon-button rc-table-icon"></i>;
      },
      onCellClick: (record: IEntry, event) => {
        if (event.target.tagName !== "I")
          return;

        this.setState({ selectedEntry: record });
        this.toggleEntryDialog();
      }
    }];
    this.onSearch = debounce(this.onSearch, 250);
  }

  componentWillReceiveProps(newProps: StoragePageProps) {
    if (this.props.params.uuid !== newProps.params.uuid)
      if (newProps.params.uuid !== undefined && newProps.params.uuid !== null)
        this.loadEntries(newProps.params.uuid)
      else
        RootStore.dispatch<IStorageAction>(StorageAction.creator.changeGroup(newProps.params.uuid));
    this.checkedRows = [];
  }

  render() {
    if (this.props.params.uuid === undefined || this.props.params.uuid === null)
      return <div className="content" id="storage"></div>;

    const entriesList = this.props.entries.filter((entry) => {
      if (this.searchInput && this.searchInput.value.length > 0)
        return entry[this.state.searchByColumn].indexOf(this.searchInput.value) > -1;
      return true;
    }).toArray();

    const visibleColumns = this.storeColumns.filter((column) => {
      if (this.props.visibleColumns[column.key] !== undefined)
        return this.props.visibleColumns[column.key];
      return true;
    });
    const searchColumns = this.storeColumns.map((element) => {
      if (element.title.length > 0)
        return (<option value={element.dataIndex} key={element.key}>{element.title}</option>);
    });

    return (
      <div className="content" id="storage">
        <div id="store-table-controls" className="pure-form">
          <input type="button" onClick={this.newEntryDialog.bind(this)}
            className="pure-button pure-button-primary" value="New profile" />
          <input type="button" onClick={this.deleteSelectedEntries.bind(this)}
            className="pure-button" value="Drop" disabled={this.state.deleteDisabled} />
          <input type="text" name="search-in-row" onKeyUp={this.onSearch.bind(this)}
            ref={input => this.searchInput = input} placeholder="Search by column"
            className="pure-input-1" />
          <select onChange={this.searchColumnChanged.bind(this)} name="row-to-search" id="row-to-search">
            {searchColumns}
          </select>
          <label>
            <input type="checkbox" onChange={this.togglePasswordsVisibility.bind(this)} />
            <span>Show passwords</span>
          </label>
          <Pagination pageChanged={this.pageChanged.bind(this)}
            itemsCount={this.state.totalEntries} itemsPerPage={this.state.entriesPerPage} />
          <Modal isOpen={this.state.entryDialogVisible} contentLabel="New profile entry dialog"
            className="new-entry-modal" overlayClassName="entry-modal-overlay">
            <i className="fa fa-times modal-close icon-button" onClick={this.toggleEntryDialog.bind(this)}></i>
            <EntryForm entry={this.state.selectedEntry} onSave={this.onEntrySaved.bind(this)} />
          </Modal>
        </div>
        <Table rowKey={record => record.id} onRowClick={this.onEntryClicked.bind(this)}
          columns={visibleColumns} data={entriesList} />
      </div>
    );
  }

  pageChanged(index: number) {
    Api.WebProfiles.GetAll(
      this.props.params.uuid,
      { count: this.state.entriesPerPage, offset: (index - 1) * this.state.entriesPerPage }
    ).then((entries: Array<IEntry>) => {
      entries.map(entry => forEachProperty(entry, ["password", "email", "username"], decrypt));
      RootStore.dispatch<IEntryAction>(EntryActions.creator.reload(List(entries)));
    }).catch(message => alertify.log(message));
  }

  togglePasswordsVisibility(event) {
    const newState = Object.assign<IStorageState, any>(this.props.visibleColumns, {
      password: event.target.checked
    });
    RootStore.dispatch<IStorageAction>(StorageAction.creator.changeVisibilityState(newState));
    this.forceUpdate();
  }

  onSearch(proxy, event) {
    this.forceUpdate();
  }

  searchColumnChanged(proxy) {
    this.setState({ searchByColumn: proxy.target.value });
  }

  newEntryDialog() {
    this.setState({
      entryDialogVisible: true,
      selectedEntry: null
    });
  }

  toggleEntryDialog() {
    this.setState({
      entryDialogVisible: !this.state.entryDialogVisible
    });
  }

  deleteSelectedEntries() {
    const deletedIndexes: Array<number> = [];
    this.checkedRows.forEach((id) => {
      const index = this.props.entries.findIndex(value => value.id === id);
      const value = this.props.entries.get(index);
      Api.WebProfiles.Delete(value.groupKey, value.id)
        .then((form) => {
          deletedIndexes.push(index);
          return form;
        }).catch((message) => {
          deletedIndexes.push(null);
          alertify.log(message);
          return message;
        }).then(() => {
          // update tableview when all requests completed
          if (deletedIndexes.length !== this.checkedRows.length)
            return;

          RootStore.dispatch<IEntryAction>(EntryActions.creator.deleteMany(deletedIndexes));
          // lock controls if all entries deleted by user
          this.setState({ deleteDisabled: true });
        });
    });
  }

  onEntryClicked(record: IEntry) {
    if (record.viewState !== undefined && record.viewState.length > 0)
      return record.viewState.pop();
  }

  onEntrySaved(form: EntryForm) {
    // check required field
    if (form.link.value.length <= 4)
      return;

    const data: IEntry = {
      groupKey: this.props.params.uuid,
      link: form.link.value,
      email: form.email.value,
      description: form.description.value,
      username: form.username.value,
      firstName: form.firstName.value,
      lastName: form.lastName.value,
      password: form.password.value
    };
    if (this.state.selectedEntry !== null)
      data.id = this.state.selectedEntry.id;

    const rpcMethod = (this.state.selectedEntry === null) ? Api.WebProfiles.New : Api.WebProfiles.Update;
    rpcMethod(data).then((entry: IEntry) => {
      let action = EntryActions.creator.newEntry(entry);
      if (this.state.selectedEntry !== null)
        action = EntryActions.creator.update(entry);

      RootStore.dispatch<IEntryAction>(action);
      // clean form and remove selection on new row or update
      this.toggleEntryDialog();
      this.setState({ selectedEntry: null });
    }).catch(message => alertify.log(message));
  }

  loadEntries(uuid: string) {
    Api.WebProfiles.Count(uuid)
      .then((data) => {
        if (data.count === undefined)
          return;

        this.setState({ totalEntries: parseInt(data.count, 10) || 0 });
        return Api.WebProfiles.GetAll(uuid, { count: this.state.entriesPerPage });
      })
      .then((data: Array<IEntry>) => {
        if (data === undefined)
          data = [];

        data.map(entry => forEachProperty(entry, ["password", "email", "username"], decrypt));
        RootStore.dispatch<IEntryAction>(EntryActions.creator.reload(List(data)));
      })
      .catch(message => alertify.log(message));
  }

  componentDidMount() {
    // init group uuid from hash uri
    RootStore.dispatch<IStorageAction>(StorageAction.creator.changeGroup(this.props.params.uuid));

    if (this.props.params.uuid !== undefined)
      this.loadEntries(this.props.params.uuid);
  }
}

function propsMapper(state: ApplicationState) {
  return {
    visibleColumns: state.storage.visibilityState,
    entries: state.entries
  };
}

export const StoragePage = connect(propsMapper)(StoragePageBase);

import * as React from "react";
import { Link } from 'react-router';
import { CProps, CState } from '../../environment';
import { LoginForm, NewUserForm } from "../forms";

export class JoinPage extends React.Component<CProps, CState> {
  render() {
    return (
      <div className="content pure-g" id="join">
        <div className="pure-u-1-5"></div>
        <div className="pure-u-1-3">
          <LoginForm />
        </div>
        <div className="pure-u-1-3">
          <NewUserForm />
        </div>
      </div>
    );
  }
}

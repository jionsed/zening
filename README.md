## About ##

It's a simple selfhosted service for manage web accounts.
Server written on typescript and node.js, api calls based on json-rpc.
See http://www.jsonrpc.org/specification for more details about this protocol.

### Installation ###

1. Setup golang $GOPATH (https://golang.org/doc/install) for server
2. Node.js for build front-end (https://nodejs.org/en/download/package-manager/#windows)

### Global depends ###

For webclient and server:

golang (1.7+)
gopm (simple package manager)
sqlite3 (as local database)
node (7+)
yarn (https://yarnpkg.com/en/docs/install, optional, npm replacement)

### Server  ###

```sh
go get -u github.com/gpmgo/gopm
gopm install
```

### Web client ###

```sh
npm install -g webpack typescript # required global bin files
webpack --watch # build webapp
```

Fix for node watcher error (ENOSPC) on linux
```sh
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```

### Gtk client deps ###
cmake libsoup-2.4-dev gtk+-3.0-dev glade libjson-glib-dev libjson-glib-1.0-0 libossp-uuid-dev uuid uuid-dev

#### Build ####
```sh
# This commands create exec file (zening) in current(build) dir
mkdir build && cd build && cmake ../ && make

# install
sudo make install
sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
```

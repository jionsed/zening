from bottle import run

from core import BottleInstance, Resource, Handler


class Account(Resource):

    name = "account"
    actions = [
        Handler("activate", "GET"),
        Handler("get_info", "GET"),
        Handler("create"),
        Handler("login"),
        Handler("update")
    ]

    def activate(self):
        return []

    def create(self):
        return []

    def login(self):
        return []

    def get_info(self):
        return []

    def update(self):
        return []


if __name__ == "__main__":
    app = BottleInstance(Account)
    run(app, host='127.0.0.1', port=8035, debug=True)

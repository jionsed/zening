import json
from bottle import Bottle, request


class BottleInstance(Bottle):

    def __init__(self, *res_list):
        super().__init__()
        self.resources = [el(self) for el in res_list]


class Handler(object):

    def __init__(self, name: str, method: str = "POST", args='', **kwargs):
        self.name = name
        self.method = method
        self.args = args

    def endpoint(self, api_point: str, res_name: str):
        p = [api_point, '/', res_name, '/', self.name]
        if self.args:
            p.append(self.args)
        return ''.join(p)


class Resource(object):

    point = "/api/rest"
    name = None
    actions = []

    def __init__(self, app: Bottle):
        for handler in self.actions:
            app.route(handler.endpoint(self.point, self.name), handler.method,
                      lambda: self.route_handler(getattr(self, handler.name)))

    def route_handler(self, cb, *args, **kwargs):
        try:
            data = cb(*args, **kwargs)
        except:
            data = {}

        return self.ok(data)

    def ok(self, data: (dict, list)):
        return json.dumps(data)

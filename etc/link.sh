#!/usr/bin/env bash
REPO_DIR=$1;
sudo rm -f /var/zening;
sudo rm -f /etc/nginx/sites-available/zening.conf;
sudo rm -f /etc/nginx/sites-enabled/zening.conf;
sudo cp -f $REPO_DIR/etc/nginx/zening.conf /etc/nginx/sites-available/zening.conf;
sudo ln -s $REPO_DIR /var/zening;
sudo ln -s /etc/nginx/sites-available/zening.conf /etc/nginx/sites-enabled;
sudo systemctl restart nginx;
